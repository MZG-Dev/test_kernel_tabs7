cmd_techpack/camera/drivers/cam_sensor_module/cam_sensor/cam_sensor_mipi.o := ../scripts/gcc-wrapper.py /home/mzg/Dokumente/TabS7+UT/tabs7-ut/build/downloads/test_kernel_tabs7/toolchain/clang/bin/clang -Wp,-MD,techpack/camera/drivers/cam_sensor_module/cam_sensor/.cam_sensor_mipi.o.d -nostdinc -isystem /home/mzg/Dokumente/TabS7+UT/tabs7-ut/build/downloads/test_kernel_tabs7/toolchain/clang/lib/clang/13.0.0/include -I../arch/arm64/include -I./arch/arm64/include/generated  -I../include -I./include -I../arch/arm64/include/uapi -I./arch/arm64/include/generated/uapi -I../include/uapi -I./include/generated/uapi -include ../include/linux/kconfig.h -include ../techpack/camera/config/konacameraconf.h -I../techpack/camera/include/uapi -I../techpack/camera/include -include ../include/linux/compiler_types.h  -I../techpack/camera/drivers/cam_sensor_module/cam_sensor -Itechpack/camera/drivers/cam_sensor_module/cam_sensor -D__KERNEL__ -mlittle-endian -DKASAN_SHADOW_SCALE_SHIFT=3 -Qunused-arguments -Wall -Wundef -Wstrict-prototypes -Wno-trigraphs -fno-strict-aliasing -fno-common -fshort-wchar -Werror-implicit-function-declaration -Wno-format-security -std=gnu89 -mcpu=cortex-a55 -fdiagnostics-color=always -pipe -Wno-void-pointer-to-enum-cast -Wno-misleading-indentation -Wno-unused-function -Wno-bool-operation -Wno-unsequenced -Wno-void-pointer-to-int-cast -Wno-unused-variable -Wno-pointer-to-int-cast -Wno-pointer-to-enum-cast -Wno-fortify-source -Wno-strlcpy-strlcat-size -Wno-align-mismatch --target=aarch64-linux-gnu --prefix=/home/mzg/Dokumente/TabS7+UT/tabs7-ut/build/downloads/test_kernel_tabs7/toolchain/gcc/bin/ --gcc-toolchain=/home/mzg/Dokumente/TabS7+UT/tabs7-ut/build/downloads/test_kernel_tabs7/toolchain/gcc -no-integrated-as -Werror=unknown-warning-option -fno-PIE -mno-implicit-float -DCONFIG_AS_LSE=1 -fno-asynchronous-unwind-tables -Wno-psabi -DKASAN_SHADOW_SCALE_SHIFT=3 -fno-delete-null-pointer-checks -Wno-frame-address -Wno-int-in-bool-context -Wno-address-of-packed-member -O3 -Wframe-larger-than=2048 -fstack-protector-strong --target=aarch64-linux-gnu --gcc-toolchain=/home/mzg/Dokumente/TabS7+UT/tabs7-ut/build/downloads/test_kernel_tabs7/toolchain/gcc -meabi gnu -Wno-format-invalid-specifier -Wno-gnu -Wno-duplicate-decl-specifier -Wno-asm-operand-widths -Wno-initializer-overrides -Wno-tautological-constant-out-of-range-compare -Wno-tautological-compare -mno-global-merge -Wno-pointer-to-enum-cast -Wno-pointer-to-int-cast -Wno-unused-const-variable -fno-omit-frame-pointer -fno-optimize-sibling-calls -Wdeclaration-after-statement -Wno-pointer-sign -fno-strict-overflow -fno-stack-check -Werror=implicit-int -Werror=strict-prototypes -Werror=date-time -Werror=incompatible-pointer-types -fmacro-prefix-map=../= -Wno-initializer-overrides -Wno-unused-value -Wno-format -Wno-sign-compare -Wno-format-zero-length -Wno-uninitialized -Wno-pointer-to-enum-cast  -I../techpack/camera/include/uapi  -I../techpack/camera/drivers/cam_core  -I../techpack/camera/drivers/cam_cpas/include  -I../techpack/camera/drivers/cam_req_mgr  -I../techpack/camera/drivers/cam_sensor_module/cam_cci  -I../techpack/camera/drivers/cam_sensor_module/cam_sensor_io  -I../techpack/camera/drivers/cam_sensor_module/cam_sensor_utils  -I../techpack/camera/drivers/cam_smmu/  -I../techpack/camera/drivers/cam_utils  -ffunction-sections -fdata-sections  -DKBUILD_BASENAME='"cam_sensor_mipi"' -DKBUILD_MODNAME='"cam_sensor_mipi"' -c -o techpack/camera/drivers/cam_sensor_module/cam_sensor/.tmp_cam_sensor_mipi.o ../techpack/camera/drivers/cam_sensor_module/cam_sensor/cam_sensor_mipi.c

source_techpack/camera/drivers/cam_sensor_module/cam_sensor/cam_sensor_mipi.o := ../techpack/camera/drivers/cam_sensor_module/cam_sensor/cam_sensor_mipi.c

deps_techpack/camera/drivers/cam_sensor_module/cam_sensor/cam_sensor_mipi.o := \
    $(wildcard include/config/camera/adaptive/mipi.h) \
    $(wildcard include/config/samsung/rear/tof.h) \
    $(wildcard include/config/samsung/front/tof.h) \
    $(wildcard include/config/camera/frs/dram/test.h) \
    $(wildcard include/config/mach/x1q/usa/single.h) \
    $(wildcard include/config/mach/y2q/kor/single.h) \
    $(wildcard include/config/sec/factory.h) \
  ../include/linux/kconfig.h \
    $(wildcard include/config/cpu/big/endian.h) \
    $(wildcard include/config/booger.h) \
    $(wildcard include/config/foo.h) \
  ../techpack/camera/config/konacameraconf.h \
    $(wildcard include/config/spectra/camera.h) \
  ../include/linux/compiler_types.h \
    $(wildcard include/config/have/arch/compiler/h.h) \
    $(wildcard include/config/enable/must/check.h) \
    $(wildcard include/config/arch/supports/optimized/inlining.h) \
    $(wildcard include/config/optimize/inlining.h) \
  ../include/linux/compiler-clang.h \
    $(wildcard include/config/cfi/clang.h) \
    $(wildcard include/config/lto/clang.h) \
    $(wildcard include/config/ftrace/mcount/record.h) \

techpack/camera/drivers/cam_sensor_module/cam_sensor/cam_sensor_mipi.o: $(deps_techpack/camera/drivers/cam_sensor_module/cam_sensor/cam_sensor_mipi.o)

$(deps_techpack/camera/drivers/cam_sensor_module/cam_sensor/cam_sensor_mipi.o):
